import { createRoot } from 'react-dom/client';

import Advice from './components/advice';

createRoot(document.querySelector('#app')).render(<Advice />);
