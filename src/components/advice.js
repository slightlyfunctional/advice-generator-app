import { useState, useEffect } from 'react';
import axios from 'axios';
import styled from 'styled-components';

const Card = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  padding-left: 15px;
  padding-right: 15px;
  padding-bottom: 20px;
  border-radius: 0.7rem;
  min-height: 17.5rem;
  width: 100%;
  max-width: 20rem;
  font-family: 'Manrope', sans-serif;
  line-height: 1.25em;
  background-color: #313a49;

  & + button {
    margin-top: -25px;
  }

  @media screen and (min-width: 1023px) {
    padding-left: 80px; 
    padding-right: 80px;
    padding-bottom: 15px;
    max-width: 33.5rem;
  }
`;

const CardHeaderText = styled.p`
  text-transform: uppercase;
  letter-spacing: 0.3em;
  font-size: 0.65rem;
  color: hsl(150, 100%, 66%);
`;

const CardContent = styled.section`
  text-align: center;
`;

const LargeText = styled.h1`
  font-size: 1.45rem;
  line-height: 1.35em;
`;

const ErrorText = styled(LargeText)`
  color: red;
`;

const QuoteText = styled(LargeText)`
  color: hsl(193, 38%, 86%); 
`;

const Button = styled.button`
  display: flex;
  justify-content: center;
  border: none;
  border-radius: 999px;
  padding: 16px;
  width: 3.5rem;

  img {
    width: 1.25rem;
    height: 1.25rem;    
  }
`;

const DisabledButton = styled(Button)`
  background-color: #000000;
  cursor: wait;
`;

const ReadyButton = styled(Button)`
  background-color: hsl(150, 100%, 66%);
  cursor: pointer;

  &:hover {
    box-shadow: 0px 0px 20px 10px rgba(82, 255, 168, 0.6);
  }
`;

export default function Advice () {
  const [quoteId, setQuoteId] = useState(-1);
  const [quote, setQuote] = useState('');
  const [disabled, setDisabled] = useState(false);
  const [error, setError] = useState(false);

  const mobileDivider = new URL('../images/pattern-divider-mobile.svg', import.meta.url);
  const desktopDivider = new URL('../images/pattern-divider-desktop.svg', import.meta.url);
  const iconDice = new URL('../images/icon-dice.svg', import.meta.url);

  const generateNewQuote = async (disableImmediately = true) => {
    let adviceDataResponse = null;
    let timeoutId = null;

    if (disableImmediately) {
      setDisabled(true);
      timeoutId = setTimeout(() => setDisabled(false), 5000); // timeout for 5 sec
    }
    
    try {
      adviceDataResponse = await generateAdvice();
    } catch (err) {
      console.error(err);
      if (timeoutId) {
        setDisabled(false);
        clearTimeout(timeoutId);
      }
      setError(true);
      setDisabled(true);
      setQuote('Unable to generate quote due to an error. Try again later.');
      return;
    }
    
    setQuoteId(adviceDataResponse.slip.id);
    setQuote(adviceDataResponse.slip.advice);
  };

  useEffect(() => {
    generateNewQuote(false);
  }, []);

  return (
    <>
      <Card>
        <header className={ !quote || error ? 'hidden' : '' }>
          <CardHeaderText>
            Advice # { quoteId || '?' }
          </CardHeaderText>
        </header>
        <CardContent>
          { error ? 
            <ErrorText>Something went wrong. Try again by refreshing the page.</ErrorText> : 
            <QuoteText>{ quote.length ? `"${quote}"` : 'Generating new quote...' }</QuoteText>
          }
        </CardContent>
        <picture>
          <source srcSet={desktopDivider} media="(min-width: 1023px)" />
          <img src={mobileDivider} alt="divider" />
        </picture>
      </Card>
      {
        disabled ? 
        <DisabledButton disabled={disabled}>
          <img src={iconDice} alt="dice" />
        </DisabledButton> : 
        <ReadyButton onClick={generateNewQuote}>
          <img src={iconDice} alt="dice" />
        </ReadyButton>
      }
    </>
  );
}

async function generateAdvice () {
  const { data } = await axios.get('https://api.adviceslip.com/advice');
  return data;
}
