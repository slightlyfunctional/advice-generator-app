# Frontend Mentor - Advice generator app solution

This is a solution to the [Advice generator app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/advice-generator-app-QdUG-13db). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Generate a new piece of advice by clicking the dice icon

### Screenshot

![Screenshot](./screenshot.png)

### Links

- Live Site URL: [Advice Generator App](https://slightlyfunctional.gitlab.io/advice-generator-app/)

## My process

### Built with

- Semantic HTML5 markup
- Flexbox
- Mobile-first workflow
- [React](https://reactjs.org/) - JS framework
- [Styled Components](https://styled-components.com) - CSS in JS framework
- [Sass](https://sass-lang.com/) - CSS Pre-processor
- [Parcel](https://parceljs.org/) - Bundler

### Continued development

I am applying BEM principles little by little, looking at examples to get an idea of how to properly name blocks and elements. This is the first implementation which I hope to improve on over time.

*Note: Second iteration - I decided to use styled-components going forward when I can with react projects.

### Useful resources

- [Free CORS proxy](https://corsproxy.io/) - This free resource was helpful to able to communicate with third party API's that don't have CORS enabled properly.
- [BEM by Example](https://sparkbox.com/foundry/bem_by_example) - This is a good article to showcase proper examples of applying BEM to CSS. 

## Author

- Website - [slightlyfunctional.com](https://slightlyfunctional.com)
- Frontend Mentor - [@slightlyfunctional](https://www.frontendmentor.io/profile/slightlyfunctional)
